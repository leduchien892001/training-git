- Sau khi đã hoàn thành clone về máy thì sử dụng visual studio code để mở project
- Tạo một nhánh mới trong project với tên nhánh được đặt theo định dạng: feature/Training_[Tên + Tên đệm viết tắt]
    VD: feature/Training_HungTK
- Chuyển sang nhánh vừa tạo để làm việc
- Trong nhánh vừa tạo tạo một file có tên đặt theo định dạng: Training_[Tên + Tên đệm viết tắt].txt
    VD: Training_HungTK.txt
- Trong file vừa mới tạo, viết lựa chọn của mình sẽ theo frontend hay backend bằng định dạng: Training_[Tên + Tên đệm viết tắt]_[Backend/Frontend]
    VD: Training_HungTK_Backend
- Push file vừa tạo lên 
* Lưu ý: 
  + Commit với cú pháp "[Tên + Tên đệm viết tắt]: Add [Tên file vừa tạo]". VD: HungTK: Add Training_HungTK.txt
  + Bước này nếu làm đúng thì sẽ nhận được lỗi "remote: You are   not allowed to push code to this project"
- Sau khi nhận được lỗi ở trên thì hãy mở mail tạo một mail với nội dung có định dạng: Training_[Tên + Tên đệm viết tắt]_[Tên username của gitlab], chủ để thì được viết với định dạng: Training_[Tên + Tên đệm viết tắt], VD: Training_HungTK  và gửi đến địa chỉ email: hungtk28102001@gmail.com
    VD: Traning_HungTK_kimhunghk (Do username gitlab của mình là kimhunghk)
- Sau khi hoàn thành bước này thì chờ gmail accept của gitlab gửi đến, chấp nhận vào project
- Chụp lại mail được gitlab  tạo một mail với định dạng nội dung: Training_[Tên + Tên đệm viết tắt]_Done, định dạng chủ đề: Training_[Tên + Tên đệm viết tắt], cộng với hình ảnh được xác nhận của gitlab và gửi đến địa chỉ email: hungtk28102001@gmail.com
- Sau khi hoàn thành các bước trên thì quay trở lại push file vừa tạo lên. Nếu như thành công thì vào team xác nhận hoàn thành task đã được giao.


